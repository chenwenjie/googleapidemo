'use strict'
import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import Camera from 'react-native-camera'

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      labels: '',
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Camera
          ref={(cam) => {
            this.camera = cam
          }}
          style={styles.preview}
          cropToPreview={true}
          captureTarget={Camera.constants.CaptureTarget.memory}
          captureQuality={Camera.constants.CaptureQuality.medium}
          playSoundOnCapture={false}
          type={Camera.constants.Type.front}
          aspect={Camera.constants.Aspect.fill}>
        </Camera>
        <View style={styles.labelContainer}>
          <Text>Detected Labels:</Text>
          <Text>{this.state.labels}</Text>
        </View>
        <View style={styles.controlBtnContainer}>
          <TouchableOpacity style={styles.controlBtn} onPress={this.start.bind(this)}>
            <Text style={{...styles.btnStart}}>Start</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.controlBtn} onPress={this.end.bind(this)}>
            <Text style={{...styles.btnEnd}}>End</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  takePicture () {
    const options = {}
    this.camera.capture()
      .then((data) => {
        this.requestLabel(data.data)
      })
      .catch(err => console.error(err))
  }

  start () {
    this.recordingId = setInterval(() => {
      this.takePicture()
    }, 1000)
  }

  end () {
    clearInterval(this.recordingId)
  }

  requestLabel (img) {
    let url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyD8ZHKdvRi-hEIzWTOBlYIevXFR0FuFO48'
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        requests: [{
          image: {content: img},
          features: [{type: 'LABEL_DETECTION'}]
        }]
      })
    }).then((resp) => resp.json())
      .then((data) => {
        this.setState({
          labels: data.responses[0].labelAnnotations.map(t => t.description).join(','),
        })
      })
      .catch((error) => {
        console.error(error)
      })

  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  labelContainer: {
    height: 100,
    padding: 20,
  },
  controlBtnContainer: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#CCCCCC',

  },
  preview: {
    flex: 1,
  },
  controlBtn: {
    flex: 1,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnStart: {
    color: 'green',
    fontSize: 20,
  },
  btnEnd: {
    color: 'red',
    fontSize: 20,
  }
}
